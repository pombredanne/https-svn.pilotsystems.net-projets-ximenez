"""Here are defined modules which are used in various plug-ins.

$Id: __init__.py 42 2007-12-01 16:00:28Z damien.baty $
"""

class ConnectionException(Exception):
    """Could not connect."""
